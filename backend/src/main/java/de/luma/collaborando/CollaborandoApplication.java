package de.luma.collaborando;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CollaborandoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollaborandoApplication.class, args);
	}

}
