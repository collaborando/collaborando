package de.luma.collaborando.rest.user;

import de.luma.collaborando.business.user.UserService;
import de.luma.collaborando.domain.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RequestMapping("/user")
@RequiredArgsConstructor
@RestController
public class UserController {
    private final UserService userService;

    @PostMapping
    ResponseEntity<String> createUser(@RequestBody UserRequestModel userRequestModel) {
        User user = userService.saveUser(userRequestModel);
        return new ResponseEntity<>("User with id " + user.getId() + " created.", HttpStatus.OK);
    }

    @GetMapping
    ResponseEntity<String> getUser(@RequestParam String userId) {
        User model = userService.findUserById(userId).orElseThrow(() ->  new ResponseStatusException(HttpStatus.NOT_FOUND));
        return new ResponseEntity<>("User model in db: " + model.toString(), HttpStatus.OK);
    }



}
