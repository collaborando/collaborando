package de.luma.collaborando.rest.user;

import lombok.Value;
import lombok.AllArgsConstructor;

@Value
@AllArgsConstructor
public class UserRequestModel {
    String username;
    String pass;
}