package de.luma.collaborando.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @GetMapping("/helloworld")
    ResponseEntity<String> getHello() {
        return new ResponseEntity<>("Hello World", HttpStatus.OK);
    }

}
