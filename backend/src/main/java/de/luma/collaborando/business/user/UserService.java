package de.luma.collaborando.business.user;

import de.luma.collaborando.domain.user.User;
import de.luma.collaborando.domain.user.UserRepository;
import de.luma.collaborando.rest.user.UserRequestModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepository;

    public User saveUser(UserRequestModel userRequestModel) {
        User user = new User();
        user.setName(userRequestModel.getUsername());
        user.setPass(userRequestModel.getPass());
        return userRepository.save(user);
    }

    public Optional<User> findUserById(String userId) {
        return userRepository.findById(userId);
    }
}
