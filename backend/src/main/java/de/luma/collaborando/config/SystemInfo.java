package de.luma.collaborando.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Component
public class SystemInfo {

    @Bean
    public void somePlaceInTheCode() throws UnknownHostException {

        System.out.println("Host address: " + InetAddress.getLocalHost().getHostAddress());
        System.out.println("Host name: " + InetAddress.getLocalHost().getHostName());
        System.out.println("Remote address: " + InetAddress.getLoopbackAddress().getHostAddress());
        System.out.println("Remote name: " + InetAddress.getLoopbackAddress().getHostName());

    }
}
