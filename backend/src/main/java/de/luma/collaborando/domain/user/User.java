package de.luma.collaborando.domain.user;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
@ToString(exclude = {"pass"})
public class User {

    @Id
    private String id;
    private String name;
    private String pass;
}
